#include <iostream>

using namespace std;

class Investment
 {
    private:  //properties
         long double value;
         float interest_rate; 
         


    public:  //methods
        
        Investment();//default constructor 
        Investment(long double , float);

		  void bank();
          void setVal(long double);
          long double getVal();
		  void setInter(float);
		  float getInter();	
 };

