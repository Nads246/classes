#include <iostream>
#include "Demo.h"

using namespace std;
Investment::Investment()
{
    value = 0.0L;
    interest_rate = 0.0;
}

 void Investment::setVal(long double var)
 {
	value = var;
 } 
 void Investment::setInter(float var2)
 {
   interest_rate = var2;
 }

 long double Investment::getVal()
 {
  return value;
 }
 Investment::Investment(long double value,float interest_rate)
{
    value = 0.0L;
    interest_rate = 0.0;
}
 void Investment::bank()
 {
	 value += (value*interest_rate);
 }
int main()
{
	Investment demo = Investment(0.083, 100.00);
	
	int count = 0;
	
	while(demo.getVal() < 1000000)
	{
		for(int i = 0; i<12; i++)
		{
			demo.setVal(demo.getVal() + 250);
		}
		demo.bank();
		count ++;	
	}
	cout << "It takes " <<count<<" years to become a millionaire"<<endl;
	return 0;
}
